package com.itheima.controller.mobile;


import com.itheima.common.constant.MessageConstant;
import com.itheima.common.constant.RedisMessageConstant;
import com.itheima.common.entity.Result;
import com.itheima.common.utils.ValidateCodeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.TimeUnit;

/**
 * 统一短信验证码
 */
@RestController
@RequestMapping("/validatecode")
public class ValidateCodeController {

    @Autowired
    private RedisTemplate redisTemplate;

    /**
     * 体检预约发送验证码
     * @param telephone
     * @return
     */
    @RequestMapping("/send4Order")
    public Result send4Order(String telephone){
        String code = ValidateCodeUtils.generateValidateCode(4).toString();
        System.out.println("验证码：" + code);
        try{
            //将验证码保存到redis，只保存5分钟   13812345678001=>3454  13812345678002=>3333
            redisTemplate.opsForValue().set(telephone + RedisMessageConstant.SENDTYPE_ORDER,code,5, TimeUnit.MINUTES);
            return new Result(true, MessageConstant.SEND_VALIDATECODE_SUCCESS);
        }catch (Exception e){
            e.printStackTrace();
            return new Result(false, MessageConstant.SEND_VALIDATECODE_FAIL);
        }
    }

    /**
     * 手机号快速登录发送验证码
     * @param telephone
     * @return
     */
    @RequestMapping("/send4Login")
    public Result send4Login(String telephone){
        String code = ValidateCodeUtils.generateValidateCode(6).toString();
        System.out.println("验证码：" + code);
        try{
            //将验证码保存到redis，只保存5分钟   13812345678001=>3454  13812345678002=>3333
            redisTemplate.opsForValue().set(telephone + RedisMessageConstant.SENDTYPE_LOGIN,code,5, TimeUnit.MINUTES);
            return new Result(true, MessageConstant.SEND_VALIDATECODE_SUCCESS);
        }catch (Exception e){
            e.printStackTrace();
            return new Result(false, MessageConstant.SEND_VALIDATECODE_FAIL);
        }
    }
}
