package com.itheima.service;

import com.itheima.common.entity.Result;
import java.util.Map;

public interface OrderService {
    public Result order(Map map) throws Exception;
    public Map findById(Integer id);
}
