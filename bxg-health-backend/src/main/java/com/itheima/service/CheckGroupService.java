package com.itheima.service;

import com.itheima.common.entity.PageResult;
import com.itheima.common.entity.QueryPageBean;
import com.itheima.pojo.CheckGroup;
import java.util.List;

public interface CheckGroupService {
    public void add(CheckGroup checkGroup, Integer[] checkitemIds);
    public PageResult findPage(QueryPageBean pageBean);
    public CheckGroup findById(Integer id);
    public void edit(CheckGroup checkGroup, Integer[] checkitemIds);
    public List<CheckGroup> findAll();
    /**
     * 根据id删除检查组
     * @param id
     * @return
     */
    void deleteById(Integer id);

    /**
     * 根据套餐id查询关联的检查项id
     * @param setmealId
     * @return
     */
    List<Integer> findCheckGroupIdsBySetmealId(Integer setmealId);
}
