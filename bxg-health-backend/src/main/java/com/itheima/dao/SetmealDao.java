package com.itheima.dao;

import com.github.pagehelper.Page;
import com.itheima.pojo.CheckGroup;
import com.itheima.pojo.CheckItem;
import com.itheima.pojo.Setmeal;

import java.util.List;
import java.util.Map;

public interface SetmealDao {
    public void add(Setmeal setmeal);
    public void setSetmealAndCheckGroup(Map<String, Integer> map);
    public Page<CheckItem> findByCondition(String queryString);
    public List<Setmeal> findAll();
    public Setmeal findById4Detail(Integer id);
    public List<Map> getSetmealReport();
    /**
     * 根据检查组id查询是否有套餐引用检查组
     * */

    int checkGroupIsUsed(Integer id);

    /**
     * 根据套餐id删除关联的检查组关系
     * @param id
     */
    void deleteAssociation(Integer id);

    /**
     * 根据id删除套餐
     * @param id
     */
    void deleteById(Integer id);

    void edit(Setmeal setmeal);
}
