package com.itheima.dao;

import com.github.pagehelper.Page;
import com.itheima.pojo.CheckGroup;

import java.util.List;
import java.util.Map;

public interface CheckGroupDao {
    public void add(CheckGroup checkGroup);
    public void setCheckGroupAndCheckItem(Map<String, Integer> map);
    public Page<CheckGroup> findByCondition(String queryString);
    public CheckGroup findById(Integer id);
    public void edit(CheckGroup checkGroup);
    public void deleteAssociation(Integer checkgroupId);
    public List<CheckGroup> findAll();

    /**
     * 根据id删除检擦组
     * @param id
     */
    void deleteById(Integer id);

    /**
     * 根据套餐id查询关联的检查项id
     * @param setmealId
     * @return
     */
    List<Integer> findCheckGroupIdsBySetmealId(Integer setmealId);
}
